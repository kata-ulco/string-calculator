<?php

declare(strict_types=1);

namespace Ulco\Tests;

use PHPUnit\Framework\TestCase;
use Ulco\Main;

class MainTest extends TestCase
{
    private ?Main $main = null;

    protected function setUp(): void
    {
        parent::setUp();
        $this->main = new Main();
    }
}
